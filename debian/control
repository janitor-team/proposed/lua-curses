Source: lua-curses
Section: interpreters
Priority: optional
Maintainer: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 9), dh-autoreconf, dh-lua, libncursesw5-dev,
 pkg-config
Standards-Version: 3.9.8
Homepage: https://github.com/lcurses/lcurses
Vcs-Git: https://anonscm.debian.org/git/pkg-lua/lua-curses.git
Vcs-Browser: https://anonscm.debian.org/cgit/lua-curses.git

Package: lua-curses
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: ${lua:Provides}
Breaks: lua-posix (<< 33.4.0)
XB-Lua-Versions: ${lua:Versions}
Description: curses library bindings for the Lua language
 This package contains bindings for the ncurses library for the Lua language.
 It allows one to build simple interfaces on a text terminal.
 .
 This package used to be part of the lua-posix source package.

Package: lua-curses-dev
Section: libdevel
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Depends: lua-curses (= ${binary:Version}), ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Development files for the lua-curses library
 This package contains the development files of the lua-curses library,
 useful to create a statically linked binary (like a C application or a
 standalone Lua interpreter).
 .
 Documentation is also shipped within this package.
